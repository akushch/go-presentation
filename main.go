package main

import "fmt"

func main() {
	s := "Hello, 世界"
	fmt.Println(s)

	for i := 0; i < len(s); i++ {
		fmt.Printf("%x %T \n", s[i], s[i])
	}
}
