package main

import "fmt"

type A struct {
	B *B
}

type B struct {
	C string
}

func main() {
	defer fmt.Println("world")
	a := new(A)
	fmt.Println(a.B.C)
}
